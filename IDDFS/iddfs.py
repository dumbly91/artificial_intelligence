class IDDFS:
    def __init__(self, fnc_next_level):
        self.fnc_next_level_ = fnc_next_level

    # from given source start and target
    def DLS(self, src, target, maxDepth):
        if src[0] == target[0] and src[1] == target[1]:
            return True, [src]
        if maxDepth <= 0:
            return False, None
        for cell in self.fnc_next_level_(src, maxDepth):
            condition, chl = self.DLS(cell, target, maxDepth - 1)
            if condition:
                return True, [src] + chl
        return False, None

    def IDDFS(self, src, target, maxDepth):
        for i in range(maxDepth):
            condition, chl = self.DLS(src, target, i)
            if condition:
                return True, chl
        return False, None