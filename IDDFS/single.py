import map.graph_map as map
import IDDFS.iddfs as iddfs
import time

if __name__ == "__main__":
    map_size = [10, 10]
    start = [4, 4]
    end = [7, 9]
    # create some obstacles
    obstacles = [dict(max=[8, 8], min=[5, 5])]
    map_handler = map.GraphMap(map_size, obstacles)
    search_handler = iddfs.IDDFS(map_handler.get_next_level)
    t_start = time.time()
    solved, solution = search_handler.IDDFS(start, end, 10)
    t_end = time.time()
    print(t_end - t_start)
    map_handler.plot()
    if solved:
        map_handler.plot_solution(solution)
    map_handler.plot_start_end(start, end)
