import map.graph_map as map
import IDDFS.iddfs as iddfs

if __name__ == "__main__":
    map_size = [10, 10]
    # agent 1
    start = [8, 4]
    end = [4, 9]
    agentes_size = 1
    agents_colors = [[1, 0, 0]]
    all_solutions = []
    # create some obstacles
    obstacles = [dict(max=[8, 8], min=[5, 5])]
    map_handler = map.GraphMap(map_size, obstacles)
    search_handler = iddfs.IDDFS(map_handler.get_next_level)
    solved, solution = search_handler.IDDFS(start, end, 10)
    map_handler.plot()
    if solved:
        all_solutions.append(solution)
        map_handler.add_agent(solution)

    # agent 2
    start_1 = [7, 9]
    end_1 = [4, 4]
    agentes_size += 1
    agents_colors.append([0, 1, 0])
    search_handler = iddfs.IDDFS(map_handler.get_next_level)
    solved, solution = search_handler.IDDFS(start_1, end_1, 10)
    if solved:
        all_solutions.append(solution)


    # agent 3
    start_1 = [4, 4]
    end_1 = [9, 9]
    agentes_size += 1
    agents_colors.append([0, 0, 1])
    search_handler = iddfs.IDDFS(map_handler.get_next_level)
    solved, solution = search_handler.IDDFS(start_1, end_1, 15)
    if solved:
        all_solutions.append(solution)


    # agent 4
    """
    start_1 = [4, 9]
    end_1 = [8, 4]
    agentes_size += 1
    agents_colors.append([0, 1, 1])
    search_handler = iddfs.IDDFS(map_handler.get_next_level)
    solved, solution = search_handler.IDDFS(start_1, end_1, 15)
    if solved:
        all_solutions.append(solution)
    """
    max_idx = max(len(subl) for subl in all_solutions)

    for idx in range(0, max_idx):
        cells = []
        for idy in range(0, agentes_size):
            agent = all_solutions[idy]
            if len(agent) > idx:
                cells.append(dict(coord=agent[idx], color=agents_colors[idy]))
            else:
                cells.append(dict(coord=agent[-1], color=agents_colors[idy]))
        map_handler.plot_solutions(cells)



