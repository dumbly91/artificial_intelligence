from tkinter import *
import backend


def god():
    root = Tk()
    root.title('PathAI')
    root.geometry('800x780')
    #root.resizable(width=FALSE, height=FALSE)
    count = 0                                           # for identifying each button/vertex and passing unique parameters
    button_list = []                                    # stores button created during runtime

    frame_up = LabelFrame(root, text='options')
    frame_down = LabelFrame(root, text='path')
    frame_up.pack()
    frame_down.pack()
    global supply_mode                                  # for differentiating b/w starting, ending & obstacles point
    supply_mode = 0
    global src                                          # src is starting point
    src = 0
    global obstacle_list                                # stores the obstacles when supply_mode is 2
    obstacle_list = []
    global dest                                         # final destination variable
    dest = 1000

    def button_mode(mode):                              # input field by user starting/obstacles/destination point
        global supply_mode
        supply_mode = mode
        print(supply_mode)

    def button_click(but_no):                           # clicked buttons in path
        #print(but_no)
        global supply_mode
        if supply_mode == 1:                                # for starting point when supply_mode = 1
            button_list[but_no].config(bg='#ffe525')
            global src
            src = but_no
            start_button['state'] = DISABLED
            supply_mode = 0
        if supply_mode == 2:                                # for obstacles      when supply_mode = 2
            button_list[but_no].config(bg='#b4b4b4')
            global obstacle_list
            obstacle_list.append(but_no)
        if supply_mode == 3:                                # for destination    when supply_mode = 3
            button_list[but_no].config(bg='#7dcf21')
            global dest
            dest=but_no
            destination_button['state'] = DISABLED
            supply_mode = 0

    start_button = Button(frame_up, text='Start point', command=lambda: button_mode(1))
    obstacle_button = Button(frame_up, text='Obstacles', command=lambda: button_mode(2))
    destination_button = Button(frame_up, text='Destination', command=lambda: button_mode(3))

    start_button.grid(row=0, column=1, sticky="ew", padx=10, pady=5)
    obstacle_button.grid(row=0, column=2, sticky="ew", padx=10, pady=5)
    destination_button.grid(row=0, column=3, sticky="ew", padx=10, pady=5)

    for i in range(20):
        for j in range(20):
            button_list.append(Button(frame_down, text=f'{count}', padx=5, pady=5, command=lambda x=count: button_click(x)))
            button_list[count].grid(row=i, column=j, sticky="ew")
            count += 1

    def solution():                                         # backend script is called
        parent = backend.backened(src, obstacle_list, dest)
        for value in parent:
            button_list[value].config(bg='#00c5ff')         # path color is turned blue
        button_list[src].config(bg='#ffe525')               # starting pt color is turned back yellow

    go_button = Button(frame_up, text='Begin', command=solution)
    go_button.grid(row=0, column=4, padx=10, pady=5)

    def restart():
        root.destroy()
        god()
        
    restart_button = Button(frame_up, text='restart', command=restart)
    restart_button.grid(row=0, column=5, padx=10, pady=5)

    def level_tut():
        already_generated_obstacle = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 39, 59, 79, 99, 119, 139, 159, 179, 199, 219, 239, 259, 279, 299, 319, 339, 359, 379, 378, 357, 336, 315, 21, 42, 63, 84, 105, 130, 150, 170, 171, 151, 131, 286, 287, 266, 267, 291, 271, 251, 201, 202, 203, 204, 155, 156, 157, 136, 176, 209, 274, 255, 236, 51, 52, 71, 72, 31, 281, 282, 283, 284, 67, 68, 87, 88, 35, 55, 75, 78, 98, 347, 348, 349, 350, 351, 352, 353, 212, 213, 214, 142, 162, 143, 163, 242, 213, 322, 323, 324, 342, 343, 344, 146, 147, 148, 167, 187, 207]
        global obstacle_list
        obstacle_list = already_generated_obstacle
        for every in already_generated_obstacle:
            button_list[every].config(bg='#b4b4b4')

    level_button = Button(frame_up, text='Delimiter', command=level_tut)
    level_button.grid(row=0, column=6, padx=10, pady=5)

    mainloop()
god()
