import map.graph_map as map
import A_star.a_star as astar
import time


if __name__ == "__main__":
    map_size = [10, 10]
    start = (4, 4)
    end = (7, 9)
    # create some obstacles
    obstacles = [dict(max=[8, 8], min=[5, 5])]
    map_handler = map.GraphMap(map_size, obstacles)
    search_handler = astar.Astar(map_handler.get_next_level)
    t_start = time.time()
    path = search_handler.search(start, end)
    t_end = time.time()
    print(t_end - t_start)
    map_handler.plot()
    if len(path):
        map_handler.plot_solution(path)
    map_handler.plot_start_end(start, end)

    print('a')

