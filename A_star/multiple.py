import map.graph_map as map
import A_star.a_star as astar
import copy
import time

if __name__ == "__main__":
    map_size = [20, 20]
    # agent 1
    start = (8, 4)
    end = (4, 9)
    agentes_size = 1
    agents_colors = [[1, 0, 0]]
    all_solutions = []
    # create some obstacles
    obstacles = [dict(max=[8, 8], min=[5, 5]), dict(max=[13, 13], min=[10, 10])]
    map_handler = map.GraphMap(map_size, obstacles)
    search_handler = astar.Astar(map_handler.get_next_level)
    path = search_handler.search(start, end)
    map_handler.plot()
    if len(path):
        all_solutions.append(copy.deepcopy(path))
        map_handler.add_agent(copy.deepcopy(path))

    # agent 2
    start_1 = (7, 9)
    end_1 = (4, 4)
    agentes_size += 1
    agents_colors.append([0, 1, 0])
    search_handler = astar.Astar(map_handler.get_next_level)
    path = search_handler.search(start_1, end_1)
    if len(path):
        all_solutions.append(copy.deepcopy(path))
        map_handler.add_agent(copy.deepcopy(path))


    # agent 3

    start_1 = (4, 4)
    end_1 = (9, 8)
    agentes_size += 1
    agents_colors.append([0, 0, 1])
    search_handler = astar.Astar(map_handler.get_next_level)
    path = search_handler.search(start_1, end_1)
    if len(path):
        all_solutions.append(copy.deepcopy(path))
        map_handler.add_agent(copy.deepcopy(path))


    # agent 4
    start_1 = (2, 2)
    end_1 = (18, 18)
    agentes_size += 1
    agents_colors.append([0, 1, 1])
    search_handler = astar.Astar(map_handler.get_next_level)
    path = search_handler.search(start_1, end_1)
    if len(path):
        all_solutions.append(copy.deepcopy(path))
        map_handler.add_agent(copy.deepcopy(path))

    # agent 5
    start_1 = (18, 2)
    end_1 = (2, 18)
    agentes_size += 1
    agents_colors.append([1, 0, 1])
    search_handler = astar.Astar(map_handler.get_next_level)
    path = search_handler.search(start_1, end_1)
    if len(path):
        all_solutions.append(copy.deepcopy(path))
        map_handler.add_agent(copy.deepcopy(path))

    max_idx = max(len(subl) for subl in all_solutions)

    for idx in range(0, max_idx):
        cells = []
        for idy in range(0, agentes_size):
            agent = all_solutions[idy]
            if len(agent) > idx:
                cells.append(dict(coord=agent[idx], color=agents_colors[idy]))
            else:
                cells.append(dict(coord=agent[-1], color=agents_colors[idy]))
        map_handler.plot_solutions(cells)



