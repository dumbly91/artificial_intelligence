import cv2
import numpy as np


def get_pose(img1, img2):
    
    
    ang = np.random.uniform(0, 2*np.pi, 3)
    tra = np.random.uniform(-1.0, 1.0, 3)
    
    Rx = np.array( ( (1.0, 0.0, 0.0), (0.0, np.cos(ang[0]), -np.sin(ang[0])), (0.0, np.sin(ang[0]), np.cos(ang[0]))))
    Ry = np.array( ( (np.cos(ang[1]), 0.0, np.sin(ang[1])), (0.0, 1.0 , 0.0), (-np.sin(ang[1]), 0.0, np.cos(ang[1]))))
    Rz = np.array( ( (np.cos(ang[2]), -np.sin(ang[2]), 0.0), (np.sin(ang[2]), np.cos(ang[2]), 0.0 ), (0.0, 0.0, 1.0) ))
    
    Rg = Rz*Ry*Rx
    T_s = np.array( ( (0.0, -tra[2], tra[1]), (tra[2], 0.0, -tra[0]), (-tra[1], tra[0], 0.0 )))
    
    E = Rg*T_s

    MIN_MATCH_COUNT = 10
    r_t = 0
    r_r = 0
    sift = cv2.xfeatures2d.SIFT_create()
    
    img1_ = arr = np.uint8(img1*255.0)
    img2_ = arr = np.uint8(img2*255.0)

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1_, None)
    kp2, des2 = sift.detectAndCompute(img2_, None)
    
    
    print(img1_pixels)
    
    # FLANN parameters
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks=50)
    
    # Perform best matching between frames using nearest neighbors
    flann = cv2.FlannBasedMatcher(index_params,search_params)
    matches = flann.knnMatch(des1,des2,k=2)
    
    good = []
    pts1 = []
    pts2 = []
    # ratio test as per Lowe's paper
    for i,(m,n) in enumerate(matches):
        if m.distance < 0.7*n.distance:
            good.append(m)
            
            #pts2.append(kp2[m.trainIdx].pt)
            #pts1.append(kp1[m.queryIdx].pt)
    
    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

        G, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 1.0)
        # Selecting the inliers alone
        src_pts = src_pts[mask.ravel() == 1]
        dst_pts = dst_pts[mask.ravel() == 1]
        
        cameraMatrix = np.array( [ [255.0/2.0, 0.0, 255.0/2.0], [0.0, 255.0/2.0, 255.0/2.0], [0.0, 0.0, 1.0]] )
        retval, rot, tras, normals = cv2.decomposeHomographyMat(G, cameraMatrix );
       
        
        for idx in range(4):
            r_t = np.linalg.norm(tras[idx])
            r_r = np.arctan2(rot[idx][2][1],rot[idx][1][1] )*180/3.14159
            #print("tras : ", r_t)
            #print("rot  : ", r_r)

    else:
        print ("Not enough matches are found - %d/%d", len(good), MIN_MATCH_COUNT )
        
    return r_t, r_r