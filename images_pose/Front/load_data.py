from __future__ import print_function, division
import os
import torch
import pandas as pd
from skimage import io, transform
import skimage
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import torchvision

# Ignore warnings
import warnings
warnings.filterwarnings("ignore")

def denormalize(tensor, mean, std):

    tensor[0] =  (std[0]*tensor[0] + mean[0])
    tensor[1] =  (std[1]*tensor[1] + mean[1])
    tensor[2] =  (std[2]*tensor[2] + mean[2])
    
    return torch.clamp(tensor, 0, 1)

class RandomCrop(object):
    """Crop randomly the images in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        image1, image2, relative = sample['image1'], sample['image2'], sample['relative']

        h, w = image1.shape[:2]
        new_h, new_w = self.output_size

        top = np.random.randint(0, h - new_h)
        left = np.random.randint(0, w - new_w)

        image1 = image1[top: top + new_h,
                      left: left + new_w]
        
        h, w = image2.shape[:2]
        new_h, new_w = self.output_size

        top = np.random.randint(0, h - new_h)
        left = np.random.randint(0, w - new_w)

        image2 = image2[top: top + new_h,
                      left: left + new_w]
       
        return {'image1': torch.from_numpy(image1),
                'image2': torch.from_numpy(image2),
                'relative': torch.from_numpy(relative)}

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        image1, image2, relative = sample['image1'], sample['image2'], sample['relative']

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        image1 = image1.transpose((2, 0, 1))
        image2 = image2.transpose((2, 0, 1))
        return {'image1': torch.from_numpy(image1),
                'image2': torch.from_numpy(image2),
                'relative': torch.from_numpy(relative)}
    
class Normalize(object):
    """Normalize a tensor image with mean and standard deviation.
    Given mean: ``(M1,...,Mn)`` and std: ``(S1,..,Sn)`` for ``n`` channels, this transform
    will normalize each channel of the input ``torch.*Tensor`` i.e.
    ``input[channel] = (input[channel] - mean[channel]) / std[channel]``
    .. note::
        This transform acts out of place, i.e., it does not mutates the input tensor.
    Args:
        mean (sequence): Sequence of means for each channel.
        std (sequence): Sequence of standard deviations for each channel.
    """

    def __init__(self, mean, std, inplace=False):
        self.mean = mean
        self.std = std
        self.inplace = inplace

    def __call__(self, sample):
        """
        Args:
            Sample that has two tensors (Tensor): Tensor images of size (C, H, W) to be normalized.
            and relative pose
        Returns:
            Sample: Normalized Tensor images.
        """
        image1, image2, relative = sample['image1'], sample['image2'], sample['relative']
            
        return {'image1': torchvision.transforms.functional.normalize(image1, self.mean, self.std ),
                'image2': torchvision.transforms.functional.normalize(image2, self.mean, self.std ),
                'relative': relative}

    def __repr__(self):
        return self.__class__.__name__ + '(mean={0}, std={1})'.format(self.mean, self.std)


class RelativePoseDataset(Dataset):
    """Relative Pose dataset."""

    def __init__(self, csv_file, root_dir, transform=None, dataset_type='train', dataset_percent=0.1):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.data = pd.read_csv(root_dir + '/'+ csv_file)
        self.root_dir = root_dir
        self.transform = transform
        
        if dataset_type == 'train' :
            self.data_init = 0
            self.max = int( (len(self.data)*(1 - dataset_percent) )/ 2)
            self.data_length = int( ( (len(self.data)*(1 - dataset_percent) ) - self.data_init) / 2 )
            
        if dataset_type == 'val' :
            self.data_init = int( len(self.data)*(1 - dataset_percent) )
            self.max = int( len(self.data) / 2 )
            self.data_length = int( (len(self.data) - self.data_init) / 2 )
            
    def __len__(self):
        return self.data_length

    def __getitem__(self, idx):
        
        #id_img = np.random.randint(self.data_init, self.max - 1 )
        img_name = os.path.join(self.root_dir, self.data.iloc[idx, 0])
        image1 = io.imread(img_name)
        image1 = skimage.img_as_float(image1)
        
        pose1 = self.data.iloc[idx, 1:].as_matrix()
        pose1 = pose1.astype('float').reshape(-1)
        
        #id_img = np.random.randint(self.data_init, self.max - 1 )
        img_name = os.path.join(self.root_dir, self.data.iloc[self.max + idx , 0])
        image2 = io.imread(img_name)
        image2 = skimage.img_as_float(image2)
        
        pose2 = self.data.iloc[self.max + idx, 1:].as_matrix()
        pose2 = pose2.astype('float').reshape(-1)
        
        distance = np.linalg.norm(pose2[:3] - pose1[:3])
        yaw = pose2[3] - pose1[3]
        relative = np.zeros(2)
        relative[0] = distance
        relative[1] = yaw
        sample = {'image1': image1, 'image2': image2, 'relative': relative}

        if self.transform:
            sample = self.transform(sample)

        return sample
    
def show_batch( sample_batched ) :
    """Show image with relative pose for a batch of samples."""
    images1_batch, images2_batch, relative_batch = \
            sample_batched['image1'], sample_batched['image2'], sample_batched['relative']
        
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
   
    batch_size = len(images1_batch)
       
    grid1 = utils.make_grid(images1_batch)
    grid1 = grid1.numpy().transpose((1, 2, 0))
    grid1 = std * grid1 + mean
    grid1 = np.clip(grid1, 0, 1)
    
    grid2 = utils.make_grid(images2_batch)
    grid2 = grid2.numpy().transpose((1, 2, 0))
    grid2 = std * grid2 + mean
    grid2 = np.clip(grid2, 0, 1)
    
    grid = np.vstack( (grid1,grid2) )
    
    plt.figure()
    plt.imshow(grid)
    
    relative_batch[:,1] = relative_batch[:,1]*180/3.14159
     
    #np.set_printoptions(precision=3)
    title=[ np.around(relative_batch[x,:].numpy(),3).tolist() for x in range(batch_size)]
    plt.title(title)
    plt.axis('off')
    plt.ioff()
    plt.show()
