import matplotlib.pyplot as plt


class GraphMap:
    def __init__(self, size, obstacles=None, agents=None):
        if obstacles is None:
            obstacles = []
        if agents is None:
            agents = []
        self.size_ = size
        self.obstacles_ = obstacles
        self.agents_ = agents
        self.patches_ = []

    def add_obstacle(self, obstacle):
        self.obstacles_.append(obstacle)

    def add_agent(self, agent):
        self.agents_.append(agent)

    def is_cell_in_map(self, cell):
        return cell[0] >= 0 and cell[0] < self.size_[0] and cell[1] >= 0 and cell[1] < self.size_[1]

    def is_cell_out_agent(self, cell, depth):
        if depth is None:
            return True
        for _, agent in enumerate(self.agents_):
            if len(agent) > depth:
                agent_cell = agent[depth]
            else:
                agent_cell = agent[-1]
            if agent_cell[0] == cell[0] and agent_cell[1] == cell[1]:
                return False
        return True


    def is_cell_out_obstacle(self, cell):
        for _, obstacle in enumerate(self.obstacles_):
            obs_max = obstacle['max']
            obs_min = obstacle['min']
            if cell[0] >= obs_min[0] and cell[0] < obs_max[0] and cell[1] >= obs_min[1] and cell[1] < obs_max[1]:
                return False
        return True

    def get_next_level(self, cell, depth=None):
        children = []
        new_cell = (cell[0] + 1, cell[1])
        if self.is_cell_in_map(new_cell) and self.is_cell_out_agent(new_cell, depth) and self.is_cell_out_obstacle(new_cell):
            children.append(new_cell)
        new_cell = (cell[0], cell[1] + 1)
        if self.is_cell_in_map(new_cell) and self.is_cell_out_agent(new_cell, depth) and self.is_cell_out_obstacle(new_cell):
            children.append(new_cell)
        new_cell = (cell[0], cell[1] - 1)
        if self.is_cell_in_map(new_cell) and self.is_cell_out_agent(new_cell, depth) and self.is_cell_out_obstacle(new_cell):
            children.append(new_cell)
        new_cell = (cell[0] - 1, cell[1])
        if self.is_cell_in_map(new_cell) and self.is_cell_out_agent(new_cell, depth) and self.is_cell_out_obstacle(new_cell):
            children.append(new_cell)
        new_cell = (cell[0] + 1, cell[1] + 1)
        if self.is_cell_in_map(new_cell) and self.is_cell_out_agent(new_cell, depth) and self.is_cell_out_obstacle(new_cell):
            children.append(new_cell)
        new_cell = (cell[0] - 1, cell[1] - 1)
        if self.is_cell_in_map(new_cell) and self.is_cell_out_agent(new_cell, depth) and self.is_cell_out_obstacle(new_cell):
            children.append(new_cell)
        new_cell = (cell[0] + 1, cell[1] - 1)
        if self.is_cell_in_map(new_cell) and self.is_cell_out_agent(new_cell, depth) and self.is_cell_out_obstacle(new_cell):
            children.append(new_cell)
        new_cell = (cell[0] - 1, cell[1] + 1)
        if self.is_cell_in_map(new_cell) and self.is_cell_out_agent(new_cell, depth) and self.is_cell_out_obstacle(new_cell):
            children.append(new_cell)
        return children

    @staticmethod
    def plot_obstacles(obstacles):
        ax = plt.gca()
        for _, obstacle in enumerate(obstacles):
            max = obstacle['max']
            min = obstacle['min']
            ax.add_patch(plt.Rectangle((min[0], min[1]), max[0] - min[0], max[1] - min[1], color='orange'))

    @staticmethod
    def plot_start_end(start, end):
        ax = plt.gca()
        ax.add_patch(plt.Rectangle((start[0], start[1]), 1, 1, color='red'))
        ax.add_patch(plt.Rectangle((end[0], end[1]), 1, 1, color='green'))

    @staticmethod
    def plot_solution(solution, color=None):
        if color is None:
            color = [0, 0, 1]
        ax = plt.gca()
        for _, cell in enumerate(solution):
            ax.add_patch(plt.Rectangle((cell[0], cell[1]), 1, 1, color=color))

    def plot(self):
        plt.figure()
        self.plot_obstacles(self.obstacles_)
        plt.gca().axis('equal')
        plt.gca().set(xlim=(-1, self.size_[0] + 2), ylim=(-1, self.size_[1] + 2))
        plt.show(block=False)

    def remove_patches(self):
        for figure_object in self.patches_:
            if isinstance(figure_object, list):
                figure_object[0].remove()
            else:
                figure_object.remove()
        self.patches_ = []

    def plot_solutions(self, cells):
        self.remove_patches()
        ax = plt.gca()
        for _, cell in enumerate(cells):
            xy_coord = cell['coord']
            printed_cell = ax.add_patch(plt.Rectangle((xy_coord[0], xy_coord[1]), 1, 1, color=cell['color']))
            self.patches_.append(printed_cell)
        plt.draw()
        plt.pause(1)

